#include <stdio.h>
#include <math.h>
#include <string.h>

int main(){
	char strng[255];
	int i,d,sum,f;
	puts("write a string:");
	fgets(strng,255,stdin);
	sum=0;
	for (d=(strnlen(strng,255)-1);d>=0;d--){
		if (strng[d]>='0' && strng[d]<='9'){
			for (i=d,f=0;strng[i]>='0' && strng[i]<='9' && i>=0;i--,f++)
				sum=sum+(strng[i]-'0')*pow(10,f);
			d=i+1;
		}
	}
	printf("%d",sum);
	return 0;
}